$(document).ready(function(){
    $(".accordion-content").hide()
    $(".load").remove()
})

$(".slider").click(function(){
    if($(this).hasClass("light")){
        $(this).removeClass("light")
        $(this).addClass("dark")
        $(".style").attr("href","static/css/style2.css")
        $(".polka").attr("src", "https://files.catbox.moe/he96cp.png")
    } else{
        $(this).removeClass("dark")
        $(this).addClass("light")
        $(".style").attr("href","static/css/style.css")
        $(".polka").attr("src", "https://files.catbox.moe/uyaws1.png")
    }
})

$(".accordion-header").click(function(){
    if($(this).hasClass("active")){
        $(this).removeClass("active")
        $(this).next(".accordion-content").slideUp()
    } else{
        $(".accordion-box .accordion-content").slideUp()
        $(".accordion-box .accordion-header").removeClass("active")
        $(this).addClass("active")
        $(this).next(".accordion-content").slideDown()
    }
})