from django.test import TestCase, Client
from django.urls import resolve
from .views import about

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time

# Create your tests here.

class UnitTestAbout(TestCase):
    def test_apakah_url_ada(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_menggunakan_fungsi_about(self):
        found = resolve('/')
        self.assertEqual(found.func, about)

    def test_apakah_fungsi_about_bekerja_saat_mendapat_request_GET_dan_menggunakan_html_about(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('about.html')

    def test_apakah_terdapat_tombol_pengubah_tema_dan_tema_default_adalah_light(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertIn('slider', content)
        self.assertIn('light', content)

    def test_apakah_ada_accordian(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertIn('accordion',content)
        self.assertIn('accordion-header',content)
        self.assertIn('accordion-content',content)
        self.assertIn('Current activities', content)
        self.assertIn('Organization and Committee', content)
        self.assertIn('Achievement', content)
    
    def test_apakah_terdapat_foto(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertIn("Lika's Photo", content)

    def test_apakah_terdapat_deskripsi(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertIn('Hi there, my name is Lika', content)

class FunctionalTestAbout(StaticLiveServerTestCase):

    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        #membuat instance dari chrome webdriver
        self.browser = webdriver.Chrome(chrome_options=options)
    
    def tearDown(self):
        self.browser.close()
    
    def test_title_sesuai(self):
        self.browser.get(self.live_server_url)
        self.assertIn("Lika", self.browser.title)

    def test_apakah_default_mode_light_bisa_berganti_menjadi_dark_mode_dan_sebaliknya(self):
        self.browser.get(self.live_server_url)
        toggle_button = self.browser.find_element_by_class_name("slider")

        #Awalnya tema yang digunakan adalah 'light' sehingga
        #css yang digunakan adalah style.css
        self.assertNotIn('dark', self.browser.page_source)
        self.assertIn('light', self.browser.page_source)
        self.assertIn('style.css', self.browser.page_source)

        #Ketika toggle button ditekan, maka berganti tema menjadi 'dark'
        #file css yang digunakan adalah style2.css
        toggle_button.click()
        self.assertNotIn('light', self.browser.page_source)
        self.assertIn('dark', self.browser.page_source)
        self.assertIn('style2.css', self.browser.page_source)

        #Ketika toggle ditekan lagi, tema kembali ke 'light'
        #css yang digunakan adalah style.css
        toggle_button.click()
        self.assertNotIn('dark', self.browser.page_source)
        self.assertIn('light', self.browser.page_source)
        self.assertIn('style.css', self.browser.page_source)
    
    def test_apakah_accordian_current_activities_dapat_dibuka(self):
        self.browser.get(self.live_server_url)

        act_content = 'name="act-content" style="display: none;"'
        act_acc = self.browser.find_element_by_name('activities')
       
        self.assertIn(act_content, self.browser.page_source)
        act_acc.click()
        self.assertNotIn(act_content, self.browser.page_source)
        

    def test_apakah_accordian_organization_and_committee_dapat_dibuka(self):
        self.browser.get(self.live_server_url)

        org_cmt_content = 'name="org-cmt-content" style="display: none;'
        org_acc = self.browser.find_element_by_name('org')

        self.assertIn(org_cmt_content, self.browser.page_source)
        org_acc.click()
        self.assertNotIn(org_cmt_content, self.browser.page_source)


    def test_apakah_accordian_achievement_dapat_dibuka(self):
        self.browser.get(self.live_server_url)

        achievement_content = 'name="achievement-content" style="display: none;"'
        ach_acc = self.browser.find_element_by_name('ach')

        self.assertIn(achievement_content, self.browser.page_source)
        ach_acc.click()
        self.assertNotIn(achievement_content, self.browser.page_source)


